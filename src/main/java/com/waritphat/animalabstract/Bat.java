/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.animalabstract;

/**
 *
 * @author domem
 */
public class Bat extends Poultry {

    private String nickname;

    public Bat(String nickname) {
        super("Bat", 2);
        this.nickname = nickname;
    }

    public void fly() {
        System.out.println("Bat: " + nickname + " can fly");
    }

    @Override
    public void eat() {
        System.out.println("Bat: " + nickname + " can eat");
    }

    @Override
    public void walk() {
        System.out.println("Bat: " + nickname + " can walk");
    }

    @Override
    public void speak() {
        System.out.println("Bat: " + nickname + " can speak");
    }

    @Override
    public void sleep() {
        System.out.println("Bat: " + nickname + " can sleep");
    }
}
