/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.animalabstract;

/**
 *
 * @author domem
 */
public class Cat extends LandAnimal{
    private String nickname;

    public Cat(String nickname) {
        super("Cat", 4);
        this.nickname = nickname;
    }

    @Override
    public void run() {
        System.out.println("Cat: " + nickname + " can run");
    }

    @Override
    public void eat() {
        System.out.println("Cat: " + nickname + " can eat");
    }

    @Override
    public void walk() {
        System.out.println("Cat: " + nickname + " can walk");
    }

    @Override
    public void speak() {
        System.out.println("Cat: " + nickname + " can speak");
    }

    @Override
    public void sleep() {
        System.out.println("Cat: " + nickname + " can sleep");
    }
    
    
}
