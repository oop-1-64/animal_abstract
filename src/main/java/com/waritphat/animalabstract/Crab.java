/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.animalabstract;

/**
 *
 * @author domem
 */
public class Crab extends AquaticAnimal{
    private String nickname;

    public Crab(String nickname) {
        super("Crab", 0);
        this.nickname = nickname;
    }

    @Override
    public void swim() {
         System.out.println("Crab: " + nickname + " can swim");
    }

    @Override
    public void eat() {
        System.out.println("Crab: " + nickname + " can eat");
    }

    @Override
    public void walk() {
        System.out.println("Crab: " + nickname + " can walk");
    }

    @Override
    public void speak() {
        System.out.println("Crab: " + nickname + " can speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crab: " + nickname + " can sleep");
    }
    
}
