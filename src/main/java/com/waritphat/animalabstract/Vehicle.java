/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.animalabstract;

/**
 *
 * @author domem
 */
public abstract class Vehicle {
    private String name;
    private int numOfWheel;

    public Vehicle(String name, int numOfWheel) {
        this.name = name;
        this.numOfWheel = numOfWheel;
    }
    public abstract void startEngine();
    public abstract void stopEngine();
}
