/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.animalabstract;

/**
 *
 * @author domem
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1 = new Human("Aum");
        h1.eat();
        h1.walk();
        h1.speak();
        h1.sleep();
        h1.run();
        Cat c1 = new Cat("Dio");
        c1.eat();
        c1.speak();
        c1.run();
        Dog d1 = new Dog("Koko");
        d1.eat();
        d1.speak();
        d1.run();
        Crocodile cr1 = new Crocodile("Dang");
        cr1.eat();
        cr1.speak();
        cr1.crawl();
        Snake s1 = new Snake("Anaconda");
        s1.eat();
        s1.speak();
        s1.crawl();
        Fish f1 = new Fish("Doctor");
        f1.eat();
        f1.speak();
        f1.swim();
        Crab crab1 = new Crab("Shelldon");
        crab1.eat();
        crab1.sleep();
        crab1.swim();
        Bat b1 = new Bat("Vampire");
        b1.eat();
        b1.sleep();
        b1.fly();
        Plane p1 = new Plane("407",3);
        p1.startEngine();
        p1.run();
        p1.fly();
        Car car1 = new Car("Toyota",4);
        car1.startEngine();
        car1.run();
        car1.stopEngine();
    }
}
