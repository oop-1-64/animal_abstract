/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.animalabstract;

/**
 *
 * @author domem
 */
public class Plane extends Vehicle implements Flyable, Runable {

    private int wheel;

    public Plane(String name, int numOfWheel) {
        super("Plane", 3);
        this.wheel = numOfWheel;

    }

    @Override
    public void startEngine() {
        System.out.println("Plane: start engine!!");
    }

    @Override
    public void stopEngine() {
        System.out.println("Plane: stop engine!!");
    }

    @Override
    public void fly() {
        System.out.println("Plane: now! it's flying");
        keepWheel();
    }

    @Override
    public void run() {
        System.out.println("Plane: It's running");
    }

    public void keepWheel() {
        System.out.println("Plane: keep " + wheel + " wheels.");
    }
}
