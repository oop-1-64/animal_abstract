/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waritphat.animalabstract;

/**
 *
 * @author domem
 */
public class Car extends Vehicle implements Runable{

    public Car(String name, int numOfWheel) {
        super("Car", 4);
    }

    @Override
    public void startEngine() {
        System.out.println("Car: start engine!!");
    }

    @Override
    public void stopEngine() {
        System.out.println("Car: stop engine!!");
    }

    @Override
    public void run() {
        System.out.println("Car: It's running.");
    }
    
}
